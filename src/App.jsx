import { useState, useEffect } from 'react';
import axios from "axios";
import './App.css';
import './components/PokemonList.css';
import './components/Pokemon.css';
import PokemonList from './components/PokemonList';
import listeDescription from './components/ListeDescription.js'
import ButtonFavorite from './components/ButtonFavorite';

const API_URL = "https://pokebuildapi.fr/api/v1/pokemon/generation/1"


function App() {
  const [pokemons, setPokemons] = useState([]);

  useEffect(() => {
    axios.get(API_URL).then((response) => {
      setPokemons(response.data)
    })
  }, [])


  return (
    <>
      <section>
        <PokemonList
          poke = { pokemons }/>
        </section>
    </>
  )
}


export default App


