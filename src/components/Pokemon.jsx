import ButtonFavorite from "./ButtonFavorite"
import '../components/Pokemon.css'

function Pokemon(props) {

    return (
        <>
            <section className="pokemon-card">
                <div className="button-heart">
                    <ButtonFavorite />
                </div>
                <img src={props.image} alt={props.name} />
                <h2>{props.name}</h2>
                <h4>Types : </h4>
                <div>

                    {/* Dans le tableau Pokemon de l'API il y a une clé apiTypes qui est un tableau d'objets*/}
                    {/* Pour récupérer les éléments dans ce tableau d'objets je fais un map*/}
                    {props.apiTypes.map((type, index) => (
                        <p key={index}>
                            {type.name}

                            {/* Vérifie si l'index ne correspond pas au dernier élément du tableau apiTypes, si oui affiche ", " si non n'affiche rien */}
                            {index !== props.apiTypes.length - 1 ? ", " : false}</p>
                    ))}
                </div>
                <p>Description:
                    {props.description}
                </p>
                
            </section>
        </>
    )
}

export default Pokemon