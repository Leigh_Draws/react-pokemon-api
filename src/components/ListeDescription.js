const listeDescription = [
    {
        id: 1,
        description: "Au matin de sa vie, la graine sur son dos et la lumière du soleil lui fournissent les éléments dont il a besoin pour grandir. Tant qu'il n'a pas fini sa croissance, il a besoin d'autres Pokémon pour survivre."
    },
    {
        id: 2,
        description: "Un bourgeon a poussé sur le dos de ce Pokémon. Pour en supporter le poids, Herbizarre a dû se muscler les pattes. Lorsqu'il commence à se prélasser au soleil, ça signifie que son bourgeon va éclore, donnant naissance à une fleur."
    },
    {
        id: 3,
        description: "Une belle fleur se trouve sur le dos de Florizarre. Elle prend une couleur vive lorsqu'elle est bien nourrie et bien ensoleillée. Le parfum de cette fleur peut apaiser les gens."
    }
]

export default listeDescription