import Pokemon from "./Pokemon";


function PokemonList(props) {
    
    const { poke } = props;

    return(
            <ul className='section-list'>
                {poke.map((pokemon) => (
                    <Pokemon
                    key={pokemon.id}
                    name={pokemon.name}
                    image={pokemon.image}
                    apiTypes={pokemon.apiTypes}
                    // description={pokemon.description}
                    />
                ))}
            </ul>
    )
}

export default PokemonList