import { useState } from "react";
import FullHeart from '../assets/heart_full.png'
import EmptyHeart from '../assets/heart_empty.png'

function ButtonFavorite () {

    const [favorite, setFavorite] = useState(false);
    
    return (
        <input type="image" alt="like" src={ favorite ? FullHeart : EmptyHeart} onClick={() => setFavorite(!favorite)} />
            
    )
    
}


export default ButtonFavorite