# Pokemon API

## 1 - Récupérer l'API Pokemon

- axios.get > récupère l'API distante 
- .then (response) > stocke l'API dans une constante response
- Utilisation du state et setState pour modifier notre tableau vide initialement et y ajouter les données (.data) de l'API

## 2 - Création d'un nouveau composant "Pokemon"

- Création d'une card
- Passage des props pour récupérer les images, noms et types de chaque élément (pokemon) de mon tableau de pokemons
- .map à l'interieur de mon tableau pour récupérer le tableau d'objets apiTypes

## 3 - Création d'une liste de composants 

- Création d'un composant qui va afficher tous les composants pokemon à partir de props
- On utilise .map sur "poke" variable qui va recevoir le tableau issu de l'API dans App.jsx
- On fait passer en props les propriétés de l'API qu'on veut récupérer 

## 4 - Appel du composant dans App

- Appel du composant PokemonList dans App
- A ce moment on peut lui donner un tableau sur lequel ils récupèrera ses props
- poke = { pokemons } ( pokemons étant le tableau issu de l'API et poke la variable dans laquelle on le stocke )

## 5 - Ajout d'un bouton like

- Création d'un composant "ButtonFavorite"
- input de type image qui change l'état du state "favorite" au clic
- Si l'état est sur "true" il affiche l'image FullHeart, si l'état est sur "false" il affiche l'image EmptyHeart

  ### A faire :
  - Récupérer une API description (ou faire un tableau description)
  - Faire passer ce tableau comme nouvelle propriété dans le tableau "Pokemons"